import { StyleSheet} from 'react-native';


export default function MainAppStyle(){
    const styles = StyleSheet.create({
        card:{
            flex:1,
            flexDirection:'row',
            borderRadius: 20,
            backgroundColor:'white',
            elevation: 1,
            shadowOffset: {width:1, height:1},
            shadowColor: 'gray',
            shadowOpacity: 0.3,
            shadowRadius:2,
            marginHorizontal:4,
            marginVertical:6,

        },

        shop_card:{
            height:160,
            borderRadius: 20,
            backgroundColor:'white',
            flexDirection:'row',
            elevation: 1,
            shadowOffset: {width:1, height:1},
            shadowColor: 'gray',
            shadowOpacity: 0.3,
            shadowRadius:2,
            marginHorizontal:4,
            marginVertical:6,
           

        },

        review_card:{
            flex:1,
            height:190,
            flexDirection:'row',
            borderRadius: 20,
            backgroundColor:'white',
            elevation: 2,
            shadowOffset: {width:1, height:1},
            shadowColor: 'gray',
            shadowOpacity: 0.3,
            shadowRadius:2,
            marginHorizontal:4,
            marginVertical:6,
            marginTop:10,
           

        },

        review_card_shop:{
            flex:1,
            height:180,
            flexDirection:'row',
            borderRadius: 20,
            backgroundColor:'white',
            elevation: 2,
            shadowOffset: {width:1, height:1},
            shadowColor: 'gray',
            shadowOpacity: 0.3,
            shadowRadius:2,
            marginHorizontal:4,
            marginVertical:6,
            marginTop:10,
           

        },

        logo:{
            resizeMode: "contain", 
            width:150, 
            height:100, 
            marginBottom:20, 
            marginLeft:10, 
            marginRight:10,
            marginTop:20,

            shadowOffset: {width:1, height:1},
            shadowColor: 'gray',
            shadowOpacity: 0.3,
            shadowRadius:2,
            marginHorizontal:4,
            marginVertical:6,
        },

        detail_logo:{
            resizeMode: "contain", 
            width:150, 
            height:100, 
            marginBottom:20, 
            marginLeft:10, 
            marginRight:10,
            marginTop:25,
            
            shadowOffset: {width:1, height:1},
            shadowColor: 'gray',
            shadowOpacity: 0.3,
            shadowRadius:2,
            marginHorizontal:4,
            marginVertical:6,
        },

        name:{
            fontSize: 30,
            fontWeight:'bold',
            maxWidth:250,
           
        },

        detail_name:{
            fontSize: 25,
            marginTop:20,
            marginBottom:5,
            fontWeight:'bold',
            maxWidth:240,
           
        },

        button:{
            color: 'pink'
        }
        
    });

    return styles
}