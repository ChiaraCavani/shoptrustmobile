import { StatusBar } from 'expo-status-bar';
import React, {Component} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import {AccountScreen} from "./screens/AccountScreen.js"
import {HomeScreen} from "./screens/HomeScreen.js"

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    
    const Stack = createStackNavigator();
    return (
        <NavigationContainer independent={true}>
            <Stack.Navigator screenOptions={{headerShown: false}}>
              <Stack.Screen name="AccountScreen" component={AccountScreen} />
              <Stack.Screen name="HomeScreen" component={HomeScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
  }
};
