import React, {Component,Fragment} from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView ,ActivityIndicator, FlatList, Item, Button, Alert, Modal, Image, TextInput, Picker,  Platform } from 'react-native';




export class AccountModifyScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
      this.state = {
        user: this.user.username,
        token:'',
        username:this.user.username,
        email:'',
        first_name:'',
        last_name:'',
        
      };
    };
  
    componentDidMount() {
      fetch('http://18.216.67.23/api/token/')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ token: json.token });
      }).done();
               
    }

    Modify() {
      const {username, email, first_name, last_name} = this.state;
      if (!username|| !email || !first_name || !last_name) Alert.alert('Error','You need to insert all the fields.');
      else {fetch(("http://18.216.67.23/api/modifyaccount/" + this.user.username +"/"), {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-CSRFToken': this.state.token
        },
        body: JSON.stringify({
          username:username,
          email:email,
          first_name:first_name,
          last_name:last_name
        })
      }).then((response) => this.doneModify(response))
      }
  }
   
    doneModify(response){
        if (response.status == '200'){
            this.props.navigation.navigate('Profile')
        }
        else {
            Alert.alert('Error','Try again!')
        }
    }


    render(){
      if (this.user.username == undefined){
        return(
          <View>
            <ActivityIndicator size="large" animating />
          </View>
        )
      }
      else{
      
      return (
        <View style={styles.container}>
         <Fragment>   
         <Image source={ require('../assets/modify2.png')} style={{ resizeMode: 'contain', width: 350, marginBottom:0 }}/>
                <TextInput
                value={this.state.username}
                onChangeText={(username) => this.setState({ username })}
                placeholder={'username'}
                maxLength={10}
                style={styles.input}
                />
                <TextInput
                multiline
                value={this.state.email}
                onChangeText={(email) => this.setState({ email })}
                placeholder={'Email'}
                style={styles.input}
                />
                <TextInput
                multiline
                value={this.state.first_name}
                onChangeText={(first_name) => this.setState({ first_name })}
                placeholder={'First name'}
                style={styles.input}
                />
                <TextInput
                multiline
                value={this.state.last_name}
                onChangeText={(last_name) => this.setState({ last_name })}
                placeholder={'Last name'}
                style={styles.input}
                />
                                  
                <Button
                title={'Submit'}
                color="#DF01D7"
                style={styles.input}
                onPress={this.Modify.bind(this)}
                />
                <Button color="#DF01D7" style={{ marginBottom:30}} title="Go back"  onPress={()=> this.props.navigation.navigate('Profile')}/> 
            </Fragment>  

        </View>
      );
        }
    }
  
  };
  

  
    
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ffffff',
    },
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 15,
      borderRadius: 10,
    },
  });