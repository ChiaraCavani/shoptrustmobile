import React, {Component} from 'react';
import { Button, View, Flat, Text, ActivityIndicator, Image, SafeAreaView, TouchableOpacity, TouchableWithoutFeedback, Animated, Linking } from 'react-native';
import { FontAwesome5, MaterialIcons } from '@expo/vector-icons'
import { FontAwesome } from '@expo/vector-icons'
import { FlatList } from 'react-native-gesture-handler';

import MainAppStyle from "../MainAppStyle";

const numStars = 5

export class ShopScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
      console.log(this.user.username)
      
      this.state = {
        details: [],
      }
    };

    renderItem = ({ item }) => { 
      let rev = []
  
      for (let x=1; x <= numStars; x++){
        rev.push(
          <TouchableWithoutFeedback key={x}>
           <Animated.View>
           <Star filled={x <= item.rating ? true : false}/>
           </Animated.View>
         </TouchableWithoutFeedback>
        );
      }
       
      return (
        <View style={ styles.review_card_shop }>
            <View style={{ marginRight:20}}>
            <Text style={{fontSize:20, marginTop:10, marginLeft:25, marginBottom:8, fontWeight: "bold"}}>{item.user}</Text>
            <View style={{ flexDirection: "row", marginLeft:10 }}>{rev}
            </View>
            <View style={{ flexDirection: "row", marginLeft:10 }}>
            <Text style={{fontSize:10, marginTop:10, marginLeft:10, marginBottom:8,}}> <FontAwesome5 name="calendar-alt" size={15} color="#161924" /> {item.rev_date}</Text>
            </View>
            </View>
            <View>
              <Text style={{fontSize:18, maxWidth:250, marginTop:10}}> { item.title} </Text>
              <Text style={{ fontSize:13, maxWidth:250, marginTop:12, marginLeft:5}}>{item.comment}</Text>
            </View>
            </View>
      )   
    }

    getDetail = async() => { 
          const { params } = this.props.route;

          await fetch(("http://18.216.67.23/api/detail/" + params.id + "/"), {
              method: 'GET'
          })
              .then(response => { return response.json(); })
              .then(responseData => { return responseData; })
              .then(data => { this.setState({ "details": data }); }) 
              .catch(err => {
                  console.log("fetch error" + err); 
              });
      }
  
    render(){
      this.getDetail()
      
      let stars = []

      if (this.state.details.shop == undefined || this.user.username == undefined){
        return(
          <View>
            <ActivityIndicator size="large" animating />
          </View>
        )
      }
      
      else{ 
      console.log(this.state.details.shop.averageRating)
      var count = Object.keys(this.state.details.review).length;
      // STARS
      for (let x=1; x <= numStars; x++){
        stars.push(
          <TouchableWithoutFeedback key={x}>
           <Animated.View>
           <Star filled={x <= this.state.details.shop.averageRating ? true : false}/>
           </Animated.View>
         </TouchableWithoutFeedback>
        );
      }
      
      return(
        <View style={{ flex:1, backgroundColor: "#FCFBFB"}}>
        <SafeAreaView style={{ flex:1, marginTop:30}}>
            <TouchableOpacity style={{alignItems: "flex-end", margin:16 }} onPress={this.props.navigation.openDrawer}>
                <FontAwesome5 name="bars" size={24} color="#161924" />
            </TouchableOpacity>
            <View>
            </View>
             <View style={styles.shop_card }>
            <Image style = { styles.detail_logo } source = {{ uri: this.state.details.shop.logo }}/>
            <View>
            <View>
              <Text style={ styles.detail_name}> { this.state.details.shop.name} </Text>
              </View>
              <View style={{ flexDirection: "row", marginBottom:10 }}>
              <Text style={{ marginLeft:5, marginTop:3 }}>Rating </Text>
              {stars}
              <Text style={{ marginTop:3, maxWidth:80 }}> ({this.state.details.shop.averageRating})</Text>
              </View>
              <View style={{ flexDirection: "row", marginLeft:5, marginBottom:10}}>
              <FontAwesome5 name="globe" size={15} color="#161924" />
              <Text style={{color: 'black',  marginLeft:5}} onPress={() => Linking.openURL(this.state.details.shop.website)}>
              Website
            </Text>
            </View>
              <Button title={'Add Review'} color="#DF01D7" style={{ marginTop: 30}} onPress={() => this.props.navigation.navigate('NewReview', {username:this.user.username, shop: this.state.details.shop.id, average:this.state.details.shop.averageRating, tot:count})}/>
            </View>
            </View>
            <Image source={ require('../assets/reviews3.png')} style={{ resizeMode: 'contain', width: 250, marginLeft:80 }}/>
            <FlatList
                    extraData={this.state}
                    data={this.state.details.review}
                    renderItem = {this.renderItem}
                />   
           <Button color="#DF01D7" style={{ marginBottom:30}} title="Back"  onPress={()=> this.props.navigation.navigate('CategoryShop')}/>       
        </SafeAreaView>
    </View>
        
      )
      }
    }
  
  };
  
class Star extends Component{
    constructor(props) {
      super(props);
    }
    render() {
      return (
        <FontAwesome 
         name={this.props.filled=== true ? "star" : "star-o"}
         size={22} 
         color="#FACC2E"
          />
      )
    }
  }

const styles = MainAppStyle()