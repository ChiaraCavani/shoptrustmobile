import React, {Component,Fragment} from 'react';
import { FontAwesome5, MaterialIcons } from '@expo/vector-icons'
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, ImageBackground } from 'react-native';

export class ContactScreen extends Component{
    constructor(props) {
      super(props);
    };
  
    render(){
      return(
        <View style={styles.container}>
                <SafeAreaView style={{ flex:1, marginTop:30}}>
                    <TouchableOpacity style={{alignItems: "flex-end", margin:16 }} onPress={this.props.navigation.openDrawer}>
                        <FontAwesome5 name="bars" size={24} color="#161924" />
                    </TouchableOpacity>
                    <View style={{ flex:1, alignItems: "center", justifyContent: "center" }}>
                    <ImageBackground source={ require('../assets/contact.png')} style={{ marginTop:150, width:420, height:680, alignItems:'center' }}/>
                    </View>
                </SafeAreaView>
            </View>
        
      )
    }
  
  };

  const styles = StyleSheet.create({
      
    container: {
        flex:1,
        backgroundColor: "#FFF",
    },
});
  