import React,{Component} from 'react';
import { Button, ActivityIndicator, Modal, FlatList, TouchableWithoutFeedback, Animated, ScrollView, TextInput, View, Text, StyleSheet, SafeAreaView,TouchableOpacity, TouchableHighlight, Image, ImageBackground } from 'react-native';
import { Feather } from "@expo/vector-icons"
import { FontAwesome } from '@expo/vector-icons'
import { FontAwesome5 } from '@expo/vector-icons'

import Icon from 'react-native-vector-icons/FontAwesome';
import MainAppStyle from "../MainAppStyle"

const numStars = 5

export class AccountProfileScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
      this.state={
        accountSource: [],
        token:'',
        done:'',
        
      }
    };

    getProfile = async() =>{
      fetch(("http://18.216.67.23/api/profile/" + this.user.username + "/"), {
        method: 'GET'
    })
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(data => { this.setState({ "accountSource": data}); }) 
        .catch(err => {
            console.log("fetch error" + err); 
        })
        .then(()=>
        fetch(("http://18.216.67.23/api/token/"), {
        method: 'GET'
    })
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(data => { this.setState({ "token": data}); }) 
        .catch(err => {
          
            console.log("fetch error" + err); 
        })
        )
      
    }

    removeButton(id){
      fetch(("http://18.216.67.23/api/deletereview/" + id ), {
        method: 'GET'
    })
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(data => { this.setState({ "done": data}); }) 
        .catch(err => {
            console.log("fetch error" + err); 
        })

    }
  
    renderItem = ({ item }) => { 
      let rev = []

      for (let x=1; x <= numStars; x++){
        rev.push(
          <TouchableWithoutFeedback key={x}>
          <Animated.View>
          <Star filled={x <= item.rating ? true : false}/>
          </Animated.View>
        </TouchableWithoutFeedback>
        );
      }
    
      return (
        <View  key={item.id} style={ mainstyles.review_card }>
            <View>
            <View style={{ flexDirection: "row"}}>
              <Text style={{fontSize:20, marginTop:14, marginLeft:5, fontWeight:'bold'}}> { item.title} </Text>
              <TouchableOpacity>
                 <View style={{ marginTop:18, marginLeft:10}}>      
                      <Icon
                        name={'trash'}
                        color={'black'}
                        size={18}
                        onPress={() => this.removeButton(item.id)}
                      />
                    </View>
                  </TouchableOpacity>
              </View>
                <Text style={{ fontSize:13, maxWidth:380, marginTop:12, marginLeft:8}}>{item.comment}</Text>
                 <View style={{ marginTop:8, marginLeft:10, flexDirection: "row" }}>{rev}</View>
                 <View style={{ flexDirection: "row", marginLeft:10 }}>
                  <Text style={{fontSize:10, marginTop:12, marginLeft:10, marginBottom:4,}}> <FontAwesome5 name="calendar-alt" size={15} color="#161924" /> {item.rev_date}</Text>
                  </View>
                  <View style={{ flexDirection: "row", marginLeft:10 }}>
                  <Text style={{fontSize:11, marginTop:5, marginLeft:8, marginBottom:8,}}> <FontAwesome5 name="store" size={15} color="#161924" /> {item.shop} </Text>
                  </View>
                    </View>
            </View>
      )   
    }

  
    render(){
      this.getProfile()
        if (this.state.accountSource.account == undefined){
            return(
              <View>
                <ActivityIndicator size="large" animating />
              </View>
            )
          }
          else{ 
            const account = this.state.accountSource.account
            var count = Object.keys(this.state.accountSource.review).length;
              return (
                  <View style={styles.container}>
                      <SafeAreaView style={{ flex:1, marginTop:30 }}>
                          <TouchableOpacity style={{alignItems: "flex-end", margin:16 }} onPress={this.props.navigation.openDrawer}>
                              <FontAwesome5 name="bars" size={24} color="#161924" />
                          </TouchableOpacity>
                          <View style={styles.card}>
                              <Image style={styles.image} source={{uri: 'http://18.216.67.23/' + account.image}}/>
                              <View style={{flex:1, }}>
                              <Text style={styles.text}>{account.first_name} {account.last_name}</Text>
                              <Feather style={{color:"grey", fontSize:15, marginLeft:12, marginTop:5, marginBottom:6}} name = "mail"><Text style={{ fontSize:15, color:"grey"}}> {account.email}</Text></Feather>
                              <Text style={{ fontSize:14, marginLeft:10, marginRight:15, marginTop:5, marginBottom:6 }}>My rate is {this.state.accountSource.average} 
                              <Feather style={{color:"white", fontSize:5, marginLeft:12, marginTop:5, marginBottom:10}} name = "star"></Feather>
                              <FontAwesome 
                              name={"star"}
                              size={18} 
                              color="#FACC2E"
                              marginLeft={30} /> over {count} reviews</Text>
                              <Button
                                title={'Modify Profile'}
                                color="#DF01D7"
                                style={{ marginTop: 10}}
                                onPress={()=> this.props.navigation.navigate('Modify', {username:this.user.username})}
                              />
                              </View>
                          </View>
                          <View>
                          <Image source={ require('../assets/reviews3.png')} style={{ resizeMode: 'contain', width: 250, marginLeft:80 }}/>
                          </View>
                          <FlatList
                          extraData={this.state}
                          data={this.state.accountSource.review}
                          renderItem = {this.renderItem}
                          keyExtractor = {(item) => {
                                 return item.id;
                                }}                         
                      /> 
                      </SafeAreaView>
                  </View>
              )
          }
    }
  
  };

  const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: "#FCFBFB",
    },

    card:{
        
        height:180,
        flexDirection:'row',
        borderRadius: 20,
        backgroundColor:'#FFF',
        elevation: 3,
        shadowOffset: {width:1, height:1},
        shadowColor: 'gray',
        shadowOpacity: 0.3,
        shadowRadius:2,
        marginHorizontal:4,
        marginVertical:6,
    },

    text: {
        color: "#161924",
        fontSize: 32,
        fontWeight: "500",
        alignItems: "center",
        justifyContent: "center",
        marginTop:20,
        marginLeft:10,
        
    },

    image:{
      width: 160, 
      height: 160, 
      borderRadius: 120, 
      marginTop:5, 
      borderWidth: 4, 
      borderColor: "white",

      
    },

})

class Star extends Component{
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <FontAwesome 
       name={this.props.filled=== true ? "star" : "star-o"}
       size={22} 
       color="#FACC2E" />
    )
  }
  
}
  

const mainstyles = MainAppStyle()