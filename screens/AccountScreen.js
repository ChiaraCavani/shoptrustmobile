import React, {Component,Fragment} from 'react';
import { StyleSheet, Text, View, Button, Alert, Image, TextInput, TouchableOpacity } from 'react-native';



export class AccountScreen extends Component {
    constructor(props) {
      super(props);
      
      this.state = {
        username: 'Chiara',
        first_name: '',
        last_name: '',
        email: '',
        password: 'Maddy170508',
        user: true,
      };
    }

    Login() {
        const { username, password } = this.state;
        if (!username || !password) Alert.alert('Error','Username and password are required');
        else {
          fetch("http://18.216.67.23/api/logout/")
          .then(()=>fetch("http://18.216.67.23/api/login/", {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: this.state.username,
            password: this.state.password
          })
        })).then((response) => this.checkLogin(response))
        .done(); 
        }
      }

    checkLogin(response){
        if (response.status == '200'){
            this.props.navigation.navigate('HomeScreen', {username:this.state.username})
        }
        else {
            Alert.alert('Error','Username or password wrong. Try again!')
        }
    }

    checkEmail = (email) => {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email)
  }

  Registration() {
      const { username, first_name, last_name, email, password} = this.state;
      if (!username || !first_name || !last_name || !email || !password) Alert.alert('Error','All the fields need to be compiled!');
      else if (!this.checkEmail(email)) Alert.alert('Error','Insert a valid email');
      else if (first_name.length > 20 || last_name.length > 20) Alert.alert('Error', "Only 20 character")
      
      else {
        fetch("http://18.216.67.23/api/logout/")
        .then(()=>fetch("http://18.216.67.23/api/register/", {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: this.state.username,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          password: this.state.password,
          email: this.state.email,
          
        })
      })).then((response) => this.checkRegistration(response))
      .done(); 
      }
  }
   
    checkRegistration(response){
        if (response.status == '200'){
            this.props.navigation.navigate('HomeScreen', {username:this.state.username})
        }
        else if (response.status == '409') Alert.alert('Error','Username or email already used.');
        else {
            Alert.alert('Error','Try again!')
        }
    }

    change(){
      this.setState({user:!this.state.user})
  }

    render() {
      
        return (
          <View style={styles.container}>
          {this.state.user 
          ?
            <Fragment>    
              <Image
                  source={require('../assets/logo.png')}
                  style={{width: 350, height: 120, marginBottom:30, marginTop:151}}
              />

              <Image source={ require('../assets/login2.png')} style={{ resizeMode: 'contain', width: 280, marginBottom:0 }}/>
              <TextInput
                  value={this.state.username}
                  onChangeText={(username) => this.setState({ username })}
                  placeholder={'Username'}
                  style={styles.input}
              />
              <TextInput
                  value={this.state.password}
                  onChangeText={(password) => this.setState({ password })}
                  placeholder={'Password'}
                  secureTextEntry={true}
                  style={styles.input}
              />
              
              <Button
                  title={'Login'}
                  color="#DF01D7"
                  style={styles.input}
                  onPress={this.Login.bind(this)}
              />

              <View style={{flexDirection: "row", marginTop:8}}>
              <Text>Don't have an account? </Text>
              <TouchableOpacity>
              <Text style={{ color:"#DF01D7"}} onPress={() => this.change()}>Register here!</Text>
              </TouchableOpacity>
              </View>

              
                    
          </Fragment>
          :(
            <Fragment>   
                  <Image
                  source={require('../assets/logo.png')}
                  style={{width: 350, height: 120, marginTop:150}}
                  />
                  <Image source={ require('../assets/register2.png')} style={{ resizeMode: 'contain', width: 280, marginBottom:0, marginTop:20 }}/>
                  <TextInput
                  value={this.state.username}
                  onChangeText={(username) => this.setState({ username })}
                  placeholder={'Username'}
                  style={styles.input}
                  />
                  <TextInput
                  value={this.state.first_name}
                  onChangeText={(first_name) => this.setState({ first_name })}
                  placeholder={'First name'}
                  style={styles.input}
                  />
                  <TextInput
                  value={this.state.last_name}
                  onChangeText={(last_name) => this.setState({ last_name })}
                  placeholder={'Last name'}
                  style={styles.input}
                  />
                  <TextInput
                  value={this.state.email}
                  onChangeText={(email) => this.setState({ email })}
                  placeholder={'Email'}
                  style={styles.input}
                  />
                  <TextInput
                  value={this.state.password}
                  onChangeText={(password) => this.setState({ password })}
                  placeholder={'Password'}
                  secureTextEntry={true}
                  style={styles.input}
                  />
                                    
                  <Button
                  title={'Register'}
                  color="#DF01D7"
                  style={styles.input}
                  onPress={this.Registration.bind(this)}
                  />

                  <View style={{flexDirection: "row", marginTop:8}}>
                    <Text>Go back to </Text>
                    <TouchableOpacity>
                    <Text style={{ color:"#DF01D7"}} onPress={() => this.change()}>Login</Text>
                    </TouchableOpacity>
                    </View>
                  
              </Fragment>  )}

          </View>
        );
          }

        }


    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        alignItems: 'center',
        
        backgroundColor: '#ffffff',
      },
      title:{
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        color: 'black',
        fontSize: 20,
        fontStyle: 'normal'
      },
      input: {
        width: 200,
        height: 44,
        padding: 10,
       
        marginBottom: 15,
        borderRadius: 10,
        backgroundColor:'#e8e8e8'
      },

      register: {
        width: 200,
        
        
      },
      button:{
        marginTop:30,
      }
    });
  

