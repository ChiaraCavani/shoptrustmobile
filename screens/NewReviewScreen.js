import React, {Component,Fragment} from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView ,ActivityIndicator, FlatList, Item, Button, Alert, Modal, Image, TextInput, Picker,  Platform } from 'react-native';


export class NewReviewScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
      this.shop = {name: this.props.route.params.id}
      this.rate = this.props.route.params.average
           
      this.state = {
        user: this.user.username,
        token:'',
        shop:'',
        title:'',
        comment:'',
        rating:'',
        profile: [],
        shopData: [],
        shopRate: '',
      };
    };

    componentDidMount() {
      fetch('http://18.216.67.23/api/token/')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ token: json.token });
      })
      .then(fetch(("http://18.216.67.23/api/profile/" + this.user.username +"/"), {
        method: 'GET'
    })
        .then(response => { return response.json(); })
        .then(responseData => { return responseData; })
        .then(data => { this.setState({ "profile": data }); }) 
        .catch(err => {
            console.log("fetch error" + err); 
        })).done();
        
       
    }

    NewReview() {
      const {id} = this.state.profile.account
      const {shop} = this.props.route.params

      const {title, comment, rating} = this.state;
      
      if (!id || !shop || !title || !comment || !rating ) Alert.alert('Error','You need to insert all the fields.');
      
      else {fetch('http://18.216.67.23/api/newreview/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-CSRFToken': this.state.token
        },
        body: JSON.stringify({
          user:id,
          shop:shop,
          title:title,
          comment:comment,
          rating: rating,
          
          
        })
      }).then((response) => this.doneReview(response))
      }
  }
   
    doneReview(response){
        if (response.status == '200'){
            this.props.navigation.navigate('ShopDetail')
        }
        else { Alert.alert('Error','Try again!') }
    }
  
    render(){
      console.log(this.state.profile.account)
      console.log(this.user.username)
      if (this.state.profile.account == undefined){
        return(
          <View>
            <ActivityIndicator size="large" animating />
          </View>
        )
      }
      else{
      return (
        <View style={styles.container}>
         <Fragment>   
            <Image source={ require('../assets/new2.png')} style={{ resizeMode: 'contain', width: 280, marginBottom:0 }}/>
                <TextInput
                value={this.state.title}
                onChangeText={(title) => this.setState({ title })}
                placeholder={'Title'}
                maxLength={10}
                style={styles.input}
                />
                <TextInput
                multiline
                value={this.state.comment}
                onChangeText={(comment) => this.setState({ comment })}
                placeholder={'Comment'}
                style={styles.comment}
                />
                <Picker
                  selectedValue={this.state.rating}
                  style={{ height: 44, width: 200 }}
                  onValueChange={(itemValue) => this.setState({rating:itemValue})}>
                  <Picker.Item label="------" value="" />
                  <Picker.Item label="1" value="1.0" />
                  <Picker.Item label="2" value="2.0" />
                  <Picker.Item label="3" value="3.0" />
                  <Picker.Item label="4" value="4.0" />
                  <Picker.Item label="5" value="5.0" />
                  </Picker>  
                <Button
                title={'Add'}
                color="#DF01D7"
                style={styles.input}
                onPress={this.NewReview.bind(this)}
                />
                <Button color="#DF01D7" style={{ marginBottom:30}} title="Go back"  onPress={()=> this.props.navigation.navigate('ShopDetail')}/> 
            </Fragment>  

        </View>
      );
        }
    }
  
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ffffff',
    },
   
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 15,
      borderRadius: 10,
    },
    comment: {
      width: 200,
      height: 80,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 15,
      borderRadius: 10,
    },

  });

  