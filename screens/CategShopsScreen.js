import React, {Component} from 'react';
import { Button, View, Text, SafeAreaView, TouchableOpacity, Image, TouchableWithoutFeedback, Animated } from 'react-native';

import { FontAwesome5 } from '@expo/vector-icons'
import { FontAwesome } from '@expo/vector-icons'
import { FlatList } from 'react-native-gesture-handler';

import MainAppStyle from "../MainAppStyle.js"

const numStars = 5

export class CategShopsScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
      this.state = {
        dataSource: [],
    }
    };
  
    // RENDER ITEM
    renderItem = ({ item }) => { 
      let stars = []

      for (let x=1; x <= numStars; x++){
        stars.push(
          <TouchableWithoutFeedback key={x}>
           <Animated.View>
           <Star filled={x <= item.averageRating ? true : false}/>
           </Animated.View>
         </TouchableWithoutFeedback>
        );
      }
     
      return (
        <TouchableOpacity key={item.id} onPress={() => this.props.navigation.navigate('ShopDetail', {id : item.id, username:this.user.username} )} style={ styles.card }>
        <Image style = { styles.logo } source = {{ uri: item.logo }}/>
        <View style= {{ justifyContent: 'center'}}>
          <Text style={ styles.name}> { item.name} </Text>
          <Text> Rating: </Text>
          <View style={{ flexDirection: "row" }}>{stars}</View>
          
        </View>
        </TouchableOpacity>
      )   
    }

    getShopsCategory(id)  { 
        
      fetch(("http://18.216.67.23/api/cat/" + id +"/"), {
          method: 'GET'
      })
          .then(response => { return response.json(); })
          .then(responseData => { return responseData; })
          .then(data => { this.setState({ refresh: false, "dataSource": data }); }) 
          .catch(err => {
              console.log("fetch error" + err); 
          });
  }

    render(){
      const { params } = this.props.route;
      // GET CATEGORY
      this.getShopsCategory(params.cat);
        return(
            <View style={{  flex:1, backgroundColor: "#FCFBFB",}}>
            <SafeAreaView style={{ flex:1, marginTop:30 }}>
            <View style={{alignItems: "flex-end", margin:16  }} >
                <TouchableOpacity onPress={this.props.navigation.openDrawer}>
                    <FontAwesome5 name="bars" size={24} color="#161924" />
                </TouchableOpacity>
                
                </View>
                <Text style={{ textAlign:"center", fontWeight:'bold', fontSize:32, marginBottom:5}}>{params.cat}</Text>
                <FlatList
                    extraData={this.state}
                    data={this.state.dataSource.shop}
                    renderItem = {this.renderItem}
                    keyExtractor={(item) => item.id}
                />          
      
                <Button color="#DF01D7" style={{ marginBottom:30}} title="Back"  onPress={()=> this.props.navigation.navigate('Category')}/>       
            </SafeAreaView>
        </View>
       
      );
    }
  
  };
  
  class Star extends Component{
    constructor(props) {
      super(props);
    }
    render() {
      return (
        <FontAwesome 
         name={this.props.filled=== true ? "star" : "star-o"}
         size={24} 
         color="#FACC2E" />
      )
    }
  }
      
  
  const styles = MainAppStyle()