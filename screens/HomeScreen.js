import React, {Component} from 'react';
import { Button, View, Text, ScrollView, ImageBackground, Image, StyleSheet, SafeAreaView, TouchableOpacity, ActivityIndicator, ListViewBase } from 'react-native';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer'
import { Feather } from "@expo/vector-icons"
import { FontAwesome5 } from '@expo/vector-icons'

import {AccountProfileScreen} from "./AccountProfileScreen.js"
import {AccountModifyScreen} from "./AccountModifyScreen.js"
import {CategoryScreen} from "./CategoryScreen.js"
import {CategShopsScreen} from "./CategShopsScreen.js"
import {ContactScreen} from "./ContactScreen.js"
import {NewReviewScreen} from "./NewReviewScreen.js"
import {ShopScreen} from "./ShopScreen.js"

export class HomeScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
    };
  
    render(){
      const Drawer = createDrawerNavigator();
      const username = this.user.username
      return(
        <Drawer.Navigator drawerContent={(props) => DrawerContent(props, username)}>
        <Drawer.Screen name="Home" component={Home}/>
        <Drawer.Screen name="Category" component={CategoryScreen}/>
        <Drawer.Screen name="CategoryShop" component={CategShopsScreen}/>
        <Drawer.Screen name="Profile" component={AccountProfileScreen}/>
        <Drawer.Screen name="Modify" component={AccountModifyScreen}/>
        <Drawer.Screen name="ShopDetail" component={ShopScreen}/>
        <Drawer.Screen name="NewReview" component={NewReviewScreen}/>
        <Drawer.Screen name="Contact" component={ContactScreen}/>
        </Drawer.Navigator>
        
      )
    }
  
  };
  
  export function DrawerContent(props,username) {
      
    return (
      <ScrollView>
          <ImageBackground source={ require('../assets/logo.png')} style={{ width: 280, paddingTop:100, marginTop:70, marginBottom:20 }}/>
           <View style={{flex:1}}>
           <Text style={{fontSize:23, marginLeft:40}} >Welcome, <Text style={{fontSize:25, fontWeight: "bold"}}>{username}!</Text></Text>
           <DrawerContentScrollView>
            
           <DrawerItem
          icon={({color, size}) => (<Feather name = "home" size={16} color={color} />)}
          label="Home"
          onPress={()=> props.navigation.navigate('Home', {username : username})}
           />
          <DrawerItem
          icon={({color, size}) => (<Feather name = "shopping-bag" size={16} color={color} />)}
          label="Shops"
          onPress={()=> props.navigation.navigate('Category', {username : username}) }
           />
          <DrawerItem
          icon={({color, size}) => (<Feather name = "user" size={16} color={color} />)}
          label="Profile"
          onPress={()=> props.navigation.navigate('Profile', {username : username})}
           />
           <DrawerItem
          icon={({color, size}) => (<Feather name = "send" size={16} color={color} />)}
          label="Contact us"
          onPress={()=> props.navigation.navigate('Contact')}
           />
            
             
           </DrawerContentScrollView>
           </View>
      </ScrollView>
    );
  }


  export class Home extends Component{
    constructor(props) {
      super(props);
      
    }
    render(){
      // <Button color="#DF01D7" title="Open Menu" onPress={this.props.navigation.openDrawer} />
      console.log(" ****HOME*****")
      return(
        <View style={Homestyles.container}>
                  <SafeAreaView style={{ flex:1, marginTop:30}}>
                      <TouchableOpacity style={{alignItems: "flex-end", margin:16 }} onPress={this.props.navigation.openDrawer}>
                          <FontAwesome5 name="bars" size={24} color="#161924" />
                      </TouchableOpacity>
                      <View style={{ flex:1, alignItems: "center", justifyContent: "center" }}>
                      <ImageBackground source={ require('../assets/home.png')} style={{ marginTop:30, width:410, height:820, alignItems:'center' }}>
                              <View style={{ marginTop:690}}>
                              <Button color="#DF01D7" title="Open Menu" onPress={this.props.navigation.openDrawer} />
                             </View>
                      </ImageBackground>
                      </View>
                  </SafeAreaView>
              </View>
      )
    }
  }
  
  const Homestyles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: "#FFF",
    },
  
    text: {
        color: "#161924",
        fontSize: 20,
        fontWeight: "500",
        alignItems: "center",
        justifyContent: "center"
    }
  
  })