
import React, {Component} from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, ActivityIndicator, ImageBackground } from 'react-native';

import { FontAwesome5 } from '@expo/vector-icons'
import { FlatList } from 'react-native-gesture-handler';

import MainAppStyle from "../MainAppStyle.js"

export class CategoryScreen extends Component{
    constructor(props) {
      super(props);
      this.user ={ username: this.props.route.params.username}
      this.state = { 
          isLoading: true,
          categoryList:[],
      }
    };
  
    renderItem = ({ item }) => { 
      const { navigate } = this.props.navigation;
      return (
        <TouchableOpacity onPress={() => navigate('CategoryShop', {cat : item.name, username:this.props.route.params.username } )} style={ styles.card }>
        <Image style = { styles.logo } source = {{ uri: item.image }}/>
        <View style= {{ justifyContent: 'center'}}>
          <Text style={ styles.name}> { item.name} </Text>
        </View>
        </TouchableOpacity>
      )   
    }

    componentDidMount() { 
      console.log("**********GET CATEG**************");
      fetch(("http://18.216.67.23/api/category/"), {
          method: 'GET'
      })
          .then(response => { return response.json(); })
          .then(responseData => { return responseData; })
          .then(data => { this.setState({ isLoading: false, "categoryList": data}); }) 
          .catch(err => {
              console.log("fetch error" + err); 
          });
    }

    render(){
      if (this.state.isLoading) {
        return (
            <View>
                <ActivityIndicator size="large" animating />
            </View>
        );
      }    
      else {
        return(
          <View style={{  flex:1, backgroundColor: "#FCFBFB",}}>
            <SafeAreaView style={{ flex:1, marginTop:30 }}>
                <TouchableOpacity style={{alignItems: "flex-end", margin:16  }} onPress={this.props.navigation.openDrawer}>
                    <FontAwesome5 name="bars" size={24} color="#161924" />
                </TouchableOpacity>
                <Image source={ require('../assets/categ3.png')} style={{ resizeMode: 'contain', width: 400, marginBottom:0 }}/>
                <FlatList
                    data={this.state.categoryList}
                    renderItem = {this.renderItem}
                    keyExtractor = {item => item.id.toString()}
                />          
            </SafeAreaView>
          </View>
          
        )
      }
    }
  
  };
  
  const styles = MainAppStyle()